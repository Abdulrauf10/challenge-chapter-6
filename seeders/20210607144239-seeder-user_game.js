'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_games', [{
      name: 'rauf',
      credit: 100,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('user_games', null, {});
  }
};
