const { urlencoded } = require('express');
const express = require('express');
const app = express();

const {user_game, user_game_biodata, user_game_history} = require('./models');

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.get('/login', (req, res) => {
    res.render('login');
  });

app.post('/login', (req, res) => {
    console.log(req.body)
    const username = req.body.username;
    const password = req.body.password;

    user_game.findAll({
        where: {
            name: username,
            credit: password
        }
    }).then(user_game => {
        if (user_game.length === 0) {
            res.redirect(301, '/login')
        }
        res.redirect(301, '/dashboard');
    })
});

app.get('/dashboard', (req, res) => {
    user_game.findAll().then(user_game => {
        res.render('dashboard', {user_game});
    })
});

app.get('/add/user', (req, res) => {
   res.render('addView')
});

app.post('/user/save', (req, res) => {
    user_game.create({
        name: req.body.name,
        credit: req.body.credit
    }).then(user_game => {
        user_game_biodata.create({
            addres: req.body.addres,
            level: req.body.level
        }).then(user_game => {
            user_game_history.create({
                win: req.body.win,
                lose: req.body.lose
            }).then(biodata => res.redirect(301, '/dashboard'))
        })
    })
})

app.get('/user/delete/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game_biodata.destroy({
      where: {
        id: userId
      }
    }).then(biodata => {
      user_game.destroy({
        where: {
          id: userId
        }
      }).then(user => {
        res.redirect(301, '/dashboard');
      })
    })
  });

  app.get('/user/update/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game.findOne({
      where: {
        id: userId
      }
    }).then(user => {
      user_game_biodata.findOne({
        where: {
          id: user.id
        }
      }).then(biodata => {
        user_game_history.findOne({
          where: {
            id: biodata.id
          }
        }).then(history => {
        res.render('update', { user, biodata, history })
      })
    })
  })
})

app.get('/api/user', (req, res) => {
  user_game.findAll().then(user_game => {
    res.status(200).json(user_game)
  })
});

app.listen(3000, () => console.log('apps berjalan di port 3000'));